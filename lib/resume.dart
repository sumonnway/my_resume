import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyResume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.commentDots,
              color: Colors.black54,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(
              Icons.more_vert,
              color: Colors.black54,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(),
          Container(
            margin: const EdgeInsets.all(16.0),
            padding: const EdgeInsets.all(16.0),
            decoration: BoxDecoration(color: Colors.grey.shade200),
            child: Text("Over 3+ years of experience in mobile applications"),
          ),
          _buildTitle("Skills"),
          SizedBox(height: 10),
          _buildSkillRow("Java", 0.8),
          SizedBox(
            height: 5.0,
          ),
          _buildSkillRow("Kotlin", 0.7),
          SizedBox(
            height: 5.0,
          ),
          _buildSkillRow("Dart", 0.6),
          SizedBox(
            height: 5.0,
          ),
          _buildSkillRow("Flutter", 0.75),
          SizedBox(
            height: 5.0,
          ),
          _buildSkillRow("Swift", 0.5),
          SizedBox(
            height: 30.0,
          ),
          _buildTitle("Experience"),
          _buildExperienceRow(
              company: "Realistic Infotech Group",
              position: "Intern Android Developer",
              duration: "May 2017 - July 2017"),
          _buildExperienceRow(
              company: "rgo47",
              position: "Mobile Developer",
              duration: "October 2017 - PRESENT"),
          SizedBox(
            height: 20.0,
          ),
          _buildTitle("Education"),
          SizedBox(
            height: 5.0,
          ),
          _buildExperienceRow(
              company: "University of Computer Studies Dawei",
              position: "B.C.Sc",
              duration: "December 2012 - September 2017"),
          SizedBox(
            height: 20.0,
          ),
          _buildTitle("Other Qualification"),
          SizedBox(
            height: 5.0,
          ),
          _buildExperienceRow(
              company:
                  "Certificated at Samsung Tech Institute Mobile Application Training",
              position: "",
              duration: "2016"),
          _buildExperienceRow(
              company: "Certificated at Internship Training",
              position: "",
              duration: "2017"),
          _buildExperienceRow(
              company:
                  "Certificated at 2016 Asia-Yangon National Programming Contest ",
              position: "",
              duration: "2016"),
          _buildExperienceRow(
              company: "Certificated of Professional iOS Developer Course ",
              position: "",
              duration: "2020"),
          SizedBox(
            height: 20.0,
          ),
          _buildTitle("Projects"),
          SizedBox(
            height: 5.0,
          ),
          _buildprojectsRow(
              logo: "", name: "Regional Product Application", duration: "2016"),
          _buildprojectsRow(
              logo: "",
              name: "Universities Directory Application",
              duration: "2016"),
          _buildprojectsRow(
              logo: "",
              name:
                  "Business Management System for Realistic Infotech Group Application",
              duration: "2017"),
          _buildprojectsRow(
              logo: "assets/images/rgo47_logo.png",
              name: "rgo47 Android App",
              duration: "2017"),
          _buildprojectsRow(
              logo: "assets/images/rgo47_plus_logo.png",
              name: "Agent Application",
              duration: "2018"),
          _buildprojectsRow(
              logo: "", name: "Seller Center Application", duration: "2018"),
          _buildprojectsRow(
              logo: "", name: "Sugar Daddy Application", duration: "2019"),
          _buildprojectsRow(
              logo: "", name: "Inventory Application", duration: "2019"),
          _buildprojectsRow(logo: "", name: "QC Application", duration: "2019"),
          _buildprojectsRow(
              logo: "", name: "Packaging Application", duration: "2019"),
          _buildprojectsRow(
              logo: "assets/images/rgo47_logo.png",
              name: "rgo47 iOS App",
              duration: "2020"),
          _buildprojectsRow(logo: "", name: "Resume App", duration: "2020"),
          SizedBox(
            height: 20.0,
          ),
          _buildTitle("Personal Details"),
          SizedBox(
            height: 5.0,
          ),
          _buildPersonalDetail(title: "Father's Name", value: "U Chit Win"),
          SizedBox(
            height: 8.0,
          ),
          _buildPersonalDetail(title: "Date of Birth", value: "09-04-1996"),
          SizedBox(
            height: 8.0,
          ),
          _buildPersonalDetail(
              title: "NRC No.", value: "6/HtaWaNa(Naing)120867"),
          SizedBox(
            height: 8.0,
          ),
          _buildPersonalDetail(title: "Nationality", value: "Myanmar"),
          SizedBox(
            height: 8.0,
          ),
          _buildPersonalDetail(title: "Marital Status", value: "Single"),
          SizedBox(
            height: 8.0,
          ),
          _buildPersonalDetail(title: "Gender", value: "Female"),
          SizedBox(
            height: 8.0,
          ),
          _buildPersonalDetail(title: "Religion", value: "Buddhism"),
          SizedBox(
            height: 25.0,
          ),
          _buildTitle("Contact"),
          SizedBox(
            height: 5.0,
          ),
          Row(
            children: <Widget>[
              SizedBox(width: 30.0),
              Icon(
                Icons.mail,
                color: Colors.black54,
              ),
              SizedBox(
                width: 10.0,
              ),
              Text("sumonnway1996@gmail.com", style: TextStyle(fontSize: 16.0))
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              SizedBox(width: 30.0),
              Icon(
                Icons.phone,
                color: Colors.black54,
              ),
              SizedBox(width: 10.0),
              Text(
                "+959 - 253-705-769",
                style: TextStyle(fontSize: 16.0),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              SizedBox(width: 30.0),
              Icon(
                Icons.map,
                color: Colors.black54,
              ),
              SizedBox(width: 10.0),
              Expanded(
                child: Text(
                  "Hlaing Tsp, Yangon",
                  style: TextStyle(fontSize: 16.0),
                ),
              )
            ],
          ),
          SizedBox(height: 50)
        ],
      )),
    );
  }

  Row _buildPersonalDetail({String title, String value}) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 16.0,
        ),
        Expanded(
            flex: 2,
            child: Text(
              title,
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
            )),
        SizedBox(
          width: 5.0,
          child: Text(":"),
        ),
        SizedBox(
          width: 5.0,
        ),
        Expanded(
            flex: 4,
            child: Text(value,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0))),
        SizedBox(
          width: 16.0,
        )
      ],
    );
  }

  ListTile _buildprojectsRow({String logo, String name, String duration}) {
    return ListTile(
      leading: Padding(
        padding: const EdgeInsets.only(top: 4.0, left: 4.0),
        child: Icon(
          FontAwesomeIcons.solidCircle,
          size: 12.0,
          color: Colors.black54,
        ),
      ),
      title: Text(
        name,
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
      subtitle: Text("($duration)"),
    );
  }

  ListTile _buildExperienceRow(
      {String company, String position, String duration}) {
    return ListTile(
      leading: Padding(
        padding: const EdgeInsets.only(top: 8.0, left: 20.0),
        child: Icon(
          FontAwesomeIcons.solidCircle,
          size: 12.0,
          color: Colors.black54,
        ),
      ),
      title: Text(
        company,
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18.0),
      ),
      subtitle: Text("$position ($duration)"),
    );
  }

  Row _buildSkillRow(String skill, double level) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 16.0,
        ),
        Expanded(
            flex: 2,
            child: Text(
              skill.toUpperCase(),
              textAlign: TextAlign.right,
            )),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            flex: 5,
            child: LinearProgressIndicator(
              value: level,
            )),
        SizedBox(
          width: 16.0,
        )
      ],
    );
  }

  Widget _buildTitle(String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title.toUpperCase(),
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          Divider(color: Colors.black54)
        ],
      ),
    );
  }

  Row _buildHeader() {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 20.0,
        ),
        Container(
          width: 80.0,
          height: 80.0,
          child: CircleAvatar(
            radius: 40,
            backgroundColor: Colors.grey,
            child: CircleAvatar(
              radius: 35.0,
              backgroundImage: AssetImage("assets/images/profile.jpeg"),
            ),
          ),
        ),
        SizedBox(
          width: 20.0,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Ma Su Mon Nway",
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Text("Senior Mobile Application Developer"),
          ],
        )
      ],
    );
  }
}
